﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using NStorage.Xml;

namespace NStorage
{
    public class Storage
    {
        private readonly Dictionary<string, IStorageCollection> collections;
        private readonly PortableFormats.PortableFormatsConfiguration portableFormats;

        public Storage(PortableFormats.PortableFormatsConfiguration portableFormatsConfiguration = null)
        {
            this.collections = new Dictionary<string, IStorageCollection>();
            this.portableFormats = (portableFormatsConfiguration ?? PortableFormats.PortableFormatsConfiguration.StandardConfiguration);
        }

        public StorageCollection AddCollection(string name)
        {
            var newCollection = new StorageCollection(name);
            this.collections.Add(name, newCollection);
            return newCollection;
        }

        public StorageCollection<T> AddCollection<T>(string name = null)
        {
            var newCollection = new StorageCollection<T>(this.portableFormats, name);
            this.collections.Add(name ?? typeof(T).FullName, newCollection);
            return newCollection;
        }

        public StorageCollection AddCollectionIfNotExists(string name)
        {
            IStorageCollection collection = null;
            return this.collections.TryGetValue(name, out collection) ? (StorageCollection)collection : AddCollection(name);
        }

        public StorageCollection<T> AddCollectionIfNotExists<T>()
        {
            IStorageCollection collection = null;
            return this.collections.TryGetValue(typeof(T).FullName, out collection) ? (StorageCollection<T>)collection : AddCollection<T>();
        }

        public void RemoveCollection(string name)
        {
            this.collections.Remove(name);
        }

        public void RemoveCollection<T>()
        {
            this.collections.Remove(typeof(T).FullName);
        }

        public StorageCollection this[string name]
        {
            get
            {
                try
                {
                    return (StorageCollection)this.collections[name];
                } catch (KeyNotFoundException)
                {
                    throw new KeyNotFoundException("Collection with name \"" + name + "\" was not found in storage.");
                }
            }
        }

        public StorageCollection<T> Get<T>(string name = null)
        {
            try
            {
                return (StorageCollection<T>)this.collections[name ?? typeof(T).FullName];
            }
            catch (KeyNotFoundException)
            {
                throw new KeyNotFoundException("Collection with name \"" + (name ?? typeof(T).FullName) + "\" was not found in storage.");
            }
        }

        public void WriteTo(TextWriter writer)
        {
            using (XmlWriter xml = XmlWriter.Create(writer))
            {
                xml.WriteTagStorage(() =>
                {
                    foreach (IStorageCollection collection in this.collections.Values)
                    {
                        collection.WriteTo(xml);
                    }
                });
            }
        }

        public void ReadFrom(TextReader reader)
        {
            using (XmlReader xml = XmlReader.Create(reader))
            {
                IStorageCollection currentCollection = null;

                xml.ReadToStorage();
                while(xml.Read())
                {
                    switch(xml.NodeType)
                    {
                        case XmlNodeType.Element:
                            if (xml.IsTagCollection())
                            {
                                currentCollection = this.collections[xml.GetCollectionName()];
                            }
                            else if (xml.IsTagItem())
                            {
                                currentCollection.ReadItem(xml);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
