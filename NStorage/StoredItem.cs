﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NStorage
{
    public class StoredItem
    {
        public Int64 ID { get; private set; }
        public IReadOnlyDictionary<string, string> Value { get; private set; }

        public StoredItem(Int64 id, IReadOnlyDictionary<string, string> value)
        {
            this.ID = id;
            this.Value = value;
        }
    }

    public class StoredItem<T>
    {
        public Int64 ID { get; private set; }
        public T Value { get; private set; }

        public StoredItem(Int64 id, T value)
        {
            this.ID = id;
            this.Value = value;
        }
    }
}
