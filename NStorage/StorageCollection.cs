﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using NStorage.Xml;

namespace NStorage
{
    public class StorageCollection : StorageCollection<Dictionary<string, string>>, IStorageCollection
    {
        public StorageCollection(string name) : base(PortableFormats.PortableFormatsConfiguration.StandardConfiguration, name)
        {
            // empty
        }

        void IStorageCollection.WriteTo(XmlWriter xml)
        {
            xml.WriteTagCollection(this.name, () =>
            {
                foreach (var item in this.items)
                {
                    xml.WriteTagItem(item.Key, () =>
                    {
                        foreach (var kv in item.Value)
                        {
                            xml.WriteElementString(kv.Key, kv.Value);
                        }
                    });
                }
            });
        }

        void IStorageCollection.ReadItem(XmlReader xml)
        {
            var currentItem = new Dictionary<string, string>();
            this.InsertOrReplace(xml.GetItemId(), currentItem);
            string currentProperty = null;
            bool readingItem = true;

            while (readingItem && xml.Read())
            {
                switch (xml.NodeType)
                {
                    case XmlNodeType.Element:
                        currentProperty = xml.Name;
                        break;
                    case XmlNodeType.Text:
                        currentItem[currentProperty] = xml.Value;
                        break;
                    case XmlNodeType.EndElement:
                        if (xml.IsTagItem())
                        {
                            readingItem = false;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
