﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NStorage.Xml
{
    static class Names
    {
        public const string Prefix = "nz";
        public const string Namespace = "http://nikitazu.ru/nstorage";

        public const string StorageTag = "storage";
        public const string CollectionTag = "collection";
        public const string ItemTag = "item";

        public const string NameAttribute = "name";
        public const string IdAttribute = "id";
        public const string NullAttribute = "isnull";

        public const string True = "true";
    }
}
