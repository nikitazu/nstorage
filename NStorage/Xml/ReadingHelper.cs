﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NStorage.Xml
{
    static class ReadingHelper
    {
        public static void ReadToStorage(this XmlReader xml)
        {
            xml.MoveToElement();
            if (!xml.ReadToFollowing(Prefixed(Names.StorageTag)))
            {
                throw new Exception("storage not found");
            }
            xml.MoveToContent();
        }

        public static string GetCollectionName(this XmlReader xml)
        {
            return xml.GetAttribute(Names.NameAttribute);
        }

        public static string GetCollectionItemTypeName(this XmlReader xml)
        {
            return xml.GetAttribute("itemType");
        }

        public static Int64 GetItemId(this XmlReader xml)
        {
            return Int64.Parse(xml.GetAttribute(Names.IdAttribute));
        }

        public static bool IsTagCollection(this XmlReader xml)
        {
            return xml.QualifiedNameEquals(Names.CollectionTag);
        }

        public static bool IsTagItem(this XmlReader xml)
        {
            return xml.QualifiedNameEquals(Names.ItemTag);
        }

        public static bool IsTagNullProperty(this XmlReader xml)
        {
            return Names.True.Equals(xml.GetAttribute(Names.NullAttribute), StringComparison.Ordinal);
        }

        private static bool QualifiedNameEquals(this XmlReader xml, string name)
        {
            return xml.NamespaceURI == Names.Namespace && xml.Name == Prefixed(name);
        }

        private static string Prefixed(string name)
        {
            return Names.Prefix + ":" + name;
        }
    }
}
