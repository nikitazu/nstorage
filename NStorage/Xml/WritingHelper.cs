﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Globalization;

namespace NStorage.Xml
{
    static class WritingHelper
    {
        public static void WriteTagStorage(this XmlWriter xml, Action doContent)
        {
            xml.WriteQualifiedTag(Names.StorageTag, doContent);
        }
        
        public static void WriteTagCollection(this XmlWriter xml, string name, Action doContent)
        {
            xml.WriteQualifiedTag(Names.CollectionTag, () =>
            {
                xml.WriteAttributeString(Names.NameAttribute, name);
                doContent();
            });
        }

        public static void WriteTagItem(this XmlWriter xml, Int64 id, Action doContent)
        {
            xml.WriteQualifiedTag(Names.ItemTag, () =>
            {
                xml.WriteAttributeString(Names.IdAttribute, id.ToString(CultureInfo.InvariantCulture));
                doContent();
            });
        }

        public static void WriteQualifiedTag(this XmlWriter xml, string name, Action doContent)
        {
            xml.WriteStartElement(Names.Prefix, name, Names.Namespace);
            doContent();
            xml.WriteEndElement();
        }

        public static void WriteTagNullProperty(this XmlWriter xml, string name)
        {
            xml.WriteStartElement(name);
            xml.WriteAttributeString(Names.NullAttribute, Names.True);
            xml.WriteEndElement();
        }
    }
}
