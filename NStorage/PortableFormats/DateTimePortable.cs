﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace NStorage.PortableFormats
{
    public class DateTimePortable : DateTimeBasePortable, IPortableFormat
    {
        public string MakeString(object source)
        {
            return ((DateTime)source).ToString(portableFormat, portableCulture);
        }

        public object ParseString(string source)
        {
            var universalButShitty = DateTime.ParseExact(source, portableFormat, portableCulture, DateTimeStyles.AdjustToUniversal);
            return DateTime.SpecifyKind(universalButShitty, DateTimeKind.Utc);
        }
    }
}
