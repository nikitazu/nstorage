﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace NStorage.PortableFormats
{
    public class DateTimeBasePortable
    {
        protected static readonly string portableFormat = CultureInfo.InvariantCulture.DateTimeFormat.UniversalSortableDateTimePattern;
        protected static readonly CultureInfo portableCulture = CultureInfo.InvariantCulture;
    }
}
