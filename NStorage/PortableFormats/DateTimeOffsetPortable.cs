﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace NStorage.PortableFormats
{
    public class DateTimeOffsetPortable : DateTimeBasePortable, IPortableFormat
    {
        public string MakeString(object source)
        {
            return ((DateTimeOffset)source).ToString(portableFormat, portableCulture);
        }

        public object ParseString(string source)
        {
            return DateTimeOffset.ParseExact(source, portableFormat, portableCulture, DateTimeStyles.AssumeUniversal);
        }
    }
}
