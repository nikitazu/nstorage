﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NStorage.PortableFormats
{
    public class PortableFormatsConfiguration
    {
        public static PortableFormatsConfiguration StandardConfiguration
        {
            get { return standardConfiguration.Value; }
        }

        public static PortableFormatsConfiguration CustomConfiguration(Dictionary<Type, Lazy<IPortableFormat>> customFormats)
        {
            var config = new PortableFormatsConfiguration();
            foreach (var kv in customFormats)
            {
                config.portableFormats[kv.Key] = kv.Value;
            }
            return config;
        }

        private static readonly Lazy<PortableFormatsConfiguration> standardConfiguration = new Lazy<PortableFormatsConfiguration>(true);

        private readonly Dictionary<Type, Lazy<IPortableFormat>> portableFormats = new Dictionary<Type, Lazy<IPortableFormat>>
        {
            { typeof(int), new Lazy<IPortableFormat>(() => new Int32Portable()) },
            { typeof(int?), new Lazy<IPortableFormat>(() => new Int32Portable()) },
            { typeof(long), new Lazy<IPortableFormat>(() => new Int64Portable()) },
            { typeof(long?), new Lazy<IPortableFormat>(() => new Int64Portable()) },
            { typeof(float), new Lazy<IPortableFormat>(() => new SinglePortable()) },
            { typeof(float?), new Lazy<IPortableFormat>(() => new SinglePortable()) },
            { typeof(decimal), new Lazy<IPortableFormat>(() => new DecimalPortable()) },
            { typeof(decimal?), new Lazy<IPortableFormat>(() => new DecimalPortable()) },
            { typeof(DateTime), new Lazy<IPortableFormat>(() => new DateTimePortable()) },
            { typeof(DateTime?), new Lazy<IPortableFormat>(() => new DateTimePortable()) },
            { typeof(DateTimeOffset), new Lazy<IPortableFormat>(() => new DateTimeOffsetPortable()) },
            { typeof(DateTimeOffset?), new Lazy<IPortableFormat>(() => new DateTimeOffsetPortable()) },
            { typeof(TimeSpan), new Lazy<IPortableFormat>(() => new TimeSpanPortable()) },
            { typeof(TimeSpan?), new Lazy<IPortableFormat>(() => new TimeSpanPortable()) },
            { typeof(string), new Lazy<IPortableFormat>(() => new StringPortable()) },
        };

        public bool TryGetValue(Type keyType, out IPortableFormat format)
        {
            Lazy<IPortableFormat> value;
            if (this.portableFormats.TryGetValue(keyType, out value))
            {
                format = value.Value;
                return true;
            }
            else
            {
                format = null;
                return false;
            }
        }
    }
}
