﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace NStorage.PortableFormats
{
    public class TimeSpanPortable : IPortableFormat
    {
        private const string portableFormat = @"hh\:mm\:ss";

        public string MakeString(object source)
        {
            return ((TimeSpan)source).ToString(portableFormat, CultureInfo.InvariantCulture);
        }

        public object ParseString(string source)
        {
            return TimeSpan.ParseExact(source, portableFormat, CultureInfo.InvariantCulture);
        }
    }
}
