﻿
/*
 * This file is generated automatically.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace NStorage.PortableFormats
{
    public class Int32Portable : IPortableFormat
    {
        public string MakeString(object source)
        {
            return ((Int32)source).ToString(CultureInfo.InvariantCulture);
        }

        public object ParseString(string source)
        {
            return Int32.Parse(source, CultureInfo.InvariantCulture);
        }
    }

    public class Int64Portable : IPortableFormat
    {
        public string MakeString(object source)
        {
            return ((Int64)source).ToString(CultureInfo.InvariantCulture);
        }

        public object ParseString(string source)
        {
            return Int64.Parse(source, CultureInfo.InvariantCulture);
        }
    }

    public class SinglePortable : IPortableFormat
    {
        public string MakeString(object source)
        {
            return ((Single)source).ToString(CultureInfo.InvariantCulture);
        }

        public object ParseString(string source)
        {
            return Single.Parse(source, CultureInfo.InvariantCulture);
        }
    }

    public class DoublePortable : IPortableFormat
    {
        public string MakeString(object source)
        {
            return ((Double)source).ToString(CultureInfo.InvariantCulture);
        }

        public object ParseString(string source)
        {
            return Double.Parse(source, CultureInfo.InvariantCulture);
        }
    }

    public class DecimalPortable : IPortableFormat
    {
        public string MakeString(object source)
        {
            return ((Decimal)source).ToString(CultureInfo.InvariantCulture);
        }

        public object ParseString(string source)
        {
            return Decimal.Parse(source, CultureInfo.InvariantCulture);
        }
    }

}
