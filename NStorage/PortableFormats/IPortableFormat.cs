﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NStorage.PortableFormats
{
    public interface IPortableFormat
    {
        string MakeString(object source);
        object ParseString(string source);
    }
}
