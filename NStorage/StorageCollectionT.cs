﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Globalization;
using System.Xml;
using NStorage.Xml;
using NStorage.PortableFormats;

namespace NStorage
{
    public class StorageCollection<T> : IStorageCollection
    {
        private readonly Dictionary<string, PropertyInfo> itemProperties;
        private readonly SequenceGenerator generator;
        private readonly PortableFormatsConfiguration portableFormats;

        protected readonly string name;
        protected readonly Dictionary<Int64, T> items = new Dictionary<Int64, T>();

        public StorageCollection(PortableFormatsConfiguration portableFormats, string name = null)
        {
            this.name = name ?? typeof(T).FullName;
            this.generator = new SequenceGenerator(0);
            this.itemProperties = typeof(T).GetRuntimeProperties().ToDictionary(p => p.Name, p => p);
            this.portableFormats = portableFormats;
        }

        public T this[Int64 id]
        {
            get
            {
                try
                {
                    return this.items[id];
                }
                catch (KeyNotFoundException)
                {
                    throw new KeyNotFoundException(string.Format(CultureInfo.InvariantCulture, "Item#{0} not found in collection \"{1}\"", id, this.name));
                }
            }
        }

        public Int64 Insert(T item)
        {
            this.items.Add(this.generator.GenerateNextNumber(), item);
            return this.generator.LastNumber;
        }

        public void InsertOrReplace(Int64 id, T newItem)
        {
            this.items[id] = newItem;
            this.generator.LastNumber = this.items.Keys.Max();
        }

        public void Remove(Int64 id)
        {
            this.items.Remove(id);
        }

        public IEnumerable<StoredItem<T>> GetAll()
        {
            return this.items.Select(kv => new StoredItem<T>(kv.Key, kv.Value));
        }

        void IStorageCollection.WriteTo(XmlWriter xml)
        {
            xml.WriteTagCollection(this.name, () =>
            {
                foreach (var item in this.items)
                {
                    xml.WriteTagItem(item.Key, () =>
                    {
                        foreach (PropertyInfo prop in this.itemProperties.Values)
                        {
                            var type = prop.PropertyType;
                            var itemValue = prop.GetValue(item.Value);
                            IPortableFormat portableFormat;

                            if (itemValue == null)
                            {
                                xml.WriteTagNullProperty(prop.Name);
                            }
                            else if (this.portableFormats.TryGetValue(type, out portableFormat))
                            {
                                xml.WriteElementString(prop.Name, portableFormat.MakeString(itemValue));
                            }
                            else
                            {
                                xml.WriteElementString(prop.Name, itemValue.ToString());
                            }
                        }
                    });
                }
            });
        }

        void IStorageCollection.ReadItem(XmlReader xml)
        {
            var currentItem = Activator.CreateInstance<T>();
            this.InsertOrReplace(xml.GetItemId(), currentItem);
            string currentPropertyName = null;
            bool readingItem = true;
            
            while (readingItem && xml.Read())
            {
                switch (xml.NodeType)
                {
                    case XmlNodeType.Element:
                        currentPropertyName = xml.Name;
                        if (xml.IsTagNullProperty())
                        {
                            this.itemProperties[currentPropertyName].SetValue(currentItem, null);
                        }
                        else if (this.itemProperties[currentPropertyName].GetMethod.ReturnType == typeof(string))
                        {
                            this.itemProperties[currentPropertyName].SetValue(currentItem, string.Empty);
                        }
                        break;
                    case XmlNodeType.Text:
                        var prop = this.itemProperties[currentPropertyName];
                        var propType = prop.GetMethod.ReturnType;
                        
                        IPortableFormat portableFormat;

                        if (this.portableFormats.TryGetValue(propType, out portableFormat))
                        {
                            prop.SetValue(currentItem, portableFormat.ParseString(xml.Value));
                        }
                        else
                        {
                            MethodInfo parse = propType.GetRuntimeMethod("Parse", new Type[] { typeof(string), typeof(IFormatProvider) });

                            if (parse == null)
                            {
                                throw new InvalidOperationException(string.Format(
                                    "Type [{0}] can't be parsed. There is no PortableFormat implementation or Parse method.", propType.FullName));
                            }
                            else
                            {
                                var parsedValue = parse.Invoke(propType, new object[] { xml.Value, CultureInfo.InvariantCulture });
                                prop.SetValue(currentItem, parsedValue);
                            }
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if (xml.IsTagItem())
                        {
                            readingItem = false;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
