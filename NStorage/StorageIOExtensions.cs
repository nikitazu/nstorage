﻿using System;
using System.IO;
using System.Text;

namespace NStorage
{
    public static class StorageIOExtensions
    {
        public static void ReadFromString(this Storage storage, string data)
        {
            using (var reader = new StringReader(data))
            {
                storage.ReadFrom(reader);
            }
        }

        public static string WriteToString(this Storage storage)
        {
            var builder = new StringBuilder();
            using (var writer = new StringWriter(builder))
            {
                storage.WriteTo(writer);
            }
            return builder.ToString();
        }
    }
}
