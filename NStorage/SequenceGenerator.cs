﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NStorage
{
    class SequenceGenerator
    {
        public Int64 LastNumber { get; internal set; }

        public SequenceGenerator(Int64 lastNumber)
        {
            this.LastNumber = lastNumber;
        }

        public Int64 GenerateNextNumber()
        {
            return ++LastNumber;
        }
    }
}
