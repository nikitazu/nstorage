﻿using System.Xml;

namespace NStorage
{
    public interface IStorageCollection
    {
        void WriteTo(XmlWriter xml);
        void ReadItem(XmlReader xml);
    }
}
