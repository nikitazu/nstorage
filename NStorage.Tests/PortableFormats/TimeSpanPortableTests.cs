﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.PortableFormats;

namespace NStorage.Tests.PortableFormats
{
    [TestClass]
    public class TimeSpanPortableTests
    {
        private readonly TimeSpanPortable portable = new TimeSpanPortable();
        private readonly TimeSpan rawData = new TimeSpan(1, 2, 3);
        private readonly string stringData = @"01:02:03";

        [TestMethod]
        public void TestMakeString()
        {
            Assert.AreEqual(this.stringData, this.portable.MakeString(this.rawData));
        }

        [TestMethod]
        public void TestParseString()
        {
            Assert.AreEqual(this.rawData, (TimeSpan)this.portable.ParseString(this.stringData));
        }
    }
}
