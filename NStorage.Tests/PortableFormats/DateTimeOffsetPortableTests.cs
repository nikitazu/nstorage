﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.PortableFormats;

namespace NStorage.Tests.PortableFormats
{
    [TestClass]
    public class DateTimeOffsetPortableTests
    {
        private readonly DateTimeOffsetPortable portable = new DateTimeOffsetPortable();
        private readonly DateTimeOffset rawData = new DateTimeOffset(2010, 12, 31, 1, 2, 3, TimeSpan.Zero);
        private readonly string stringData = @"2010-12-31 01:02:03Z";

        [TestMethod]
        public void TestMakeString()
        {
            Assert.AreEqual(this.stringData, this.portable.MakeString(this.rawData));
        }

        [TestMethod]
        public void TestParseString()
        {
            var parsedData = (DateTimeOffset)this.portable.ParseString(this.stringData);
            Assert.AreEqual(TimeSpan.Zero, parsedData.Offset);
            Assert.AreEqual(this.rawData, parsedData);
        }
    }
}
