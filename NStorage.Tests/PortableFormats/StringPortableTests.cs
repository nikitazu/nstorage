﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.PortableFormats;

namespace NStorage.Tests.PortableFormats
{
    [TestClass]
    public class StringPortableTests
    {
        private readonly StringPortable portable = new StringPortable();
        private readonly string rawData = "foo";
        private readonly string stringData = @"foo";

        [TestMethod]
        public void TestMakeString()
        {
            Assert.AreEqual(this.stringData, this.portable.MakeString(this.rawData));
        }

        [TestMethod]
        public void TestParseString()
        {
            Assert.AreEqual(this.rawData, this.portable.ParseString(this.stringData));
        }
    }
}
