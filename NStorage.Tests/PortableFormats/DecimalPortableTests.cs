﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.PortableFormats;

namespace NStorage.Tests.PortableFormats
{
    [TestClass]
    public class DecimalPortableTests
    {
        private readonly DecimalPortable portable = new DecimalPortable();
        private readonly decimal rawData = 12345.45m;
        private readonly string stringData = @"12345.45";

        [TestMethod]
        public void TestMakeString()
        {
            Assert.AreEqual(this.stringData, this.portable.MakeString(this.rawData));
        }

        [TestMethod]
        public void TestParseString()
        {
            Assert.AreEqual(this.rawData, this.portable.ParseString(this.stringData));
        }
    }
}
