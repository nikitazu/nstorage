﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.PortableFormats;

namespace NStorage.Tests.PortableFormats
{
    [TestClass]
    public class IntPortableTests
    {
        private readonly Int32Portable portable = new Int32Portable();
        private readonly int rawData = 12345;
        private readonly string stringData = @"12345";

        [TestMethod]
        public void TestMakeString()
        {
            Assert.AreEqual(this.stringData, this.portable.MakeString(this.rawData));
        }

        [TestMethod]
        public void TestParseString()
        {
            Assert.AreEqual(this.rawData, this.portable.ParseString(this.stringData));
        }
    }
}
