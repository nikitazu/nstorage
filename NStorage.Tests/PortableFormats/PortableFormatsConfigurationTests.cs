﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.PortableFormats;

namespace NStorage.Tests.PortableFormats
{
    [TestClass]
    public class MyTestClass
    {
        const string storageDataTyped = @"<?xml version=""1.0"" encoding=""utf-16""?><nz:storage xmlns:nz=""http://nikitazu.ru/nstorage""><nz:collection name=""NStorage.Tests.Foo""><nz:item id=""1""><Bar>uzatikin</Bar></nz:item></nz:collection></nz:storage>";

        class StringReversingPortable : IPortableFormat
        {
            public string MakeString(object source)
            {
                return string.Join(string.Empty, ((string)source).Reverse());
            }

            public object ParseString(string source)
            {
                return MakeString(source);
            }
        }

        Storage storage;
        StorageCollection<Foo> foos;
        Foo foo;

        [TestInitialize]
        public void Init()
        {
            this.storage = new Storage(PortableFormatsConfiguration.CustomConfiguration(new Dictionary<Type, Lazy<IPortableFormat>>
            {
                { typeof(string), new Lazy<IPortableFormat>(() => new StringReversingPortable()) }
            }));
            this.foos = storage.AddCollection<Foo>();
            this.foo = new Foo { Bar = "nikitazu" };
        }

        [TestMethod]
        public void TestWriteStorageWithCustomPortableFormatsConfiguration()
        {
            this.foos.Insert(foo);
            Assert.AreEqual(storageDataTyped, this.storage.WriteToString());
        }

        [TestMethod]
        public void TestReadStorageWithCustomPortableFormatsConfiguration()
        {
            this.storage.ReadFromString(storageDataTyped);
            Assert.AreEqual(this.foo.Bar, this.foos[1].Bar);
        }
    }
}
