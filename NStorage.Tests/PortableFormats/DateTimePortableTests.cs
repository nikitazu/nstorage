﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.PortableFormats;

namespace NStorage.Tests.PortableFormats
{
    [TestClass]
    public class DateTimePortableTests
    {
        private readonly DateTimePortable portable = new DateTimePortable();
        private readonly DateTime rawData = new DateTime(2010, 12, 31, 1, 2, 3, DateTimeKind.Utc);
        private readonly string stringData = @"2010-12-31 01:02:03Z";

        [TestMethod]
        public void TestMakeString()
        {
            Assert.AreEqual(this.stringData, this.portable.MakeString(this.rawData));
        }

        [TestMethod]
        public void TestParseString()
        {
            var parsedData = (DateTime)this.portable.ParseString(this.stringData);
            Assert.AreEqual(DateTimeKind.Utc, parsedData.Kind);
            Assert.AreEqual(this.rawData, parsedData);
        }
    }
}
