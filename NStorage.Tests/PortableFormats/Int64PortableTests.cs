﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.PortableFormats;

namespace NStorage.Tests.PortableFormats
{
    [TestClass]
    public class Int64PortableTests
    {
        private readonly Int64Portable portable = new Int64Portable();
        private readonly Int64 rawData = 1234567890123456789;
        private readonly string stringData = @"1234567890123456789";

        [TestMethod]
        public void TestMakeString()
        {
            Assert.AreEqual(this.stringData, this.portable.MakeString(this.rawData));
        }

        [TestMethod]
        public void TestParseString()
        {
            Assert.AreEqual(this.rawData, this.portable.ParseString(this.stringData));
        }
    }
}
