﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NStorage.Tests.Migration
{
    [TestClass]
    public class MigratorTests : StorageTestBase
    {
        const string storageData = @"<?xml version=""1.0"" encoding=""utf-16""?><nz:storage xmlns:nz=""http://nikitazu.ru/nstorage""><nz:collection name=""myItems""><nz:item id=""1""><name>niki</name><age>20</age></nz:item><nz:item id=""2""><name>alex</name><age>30</age></nz:item></nz:collection><nz:collection name=""jobs""><nz:item id=""1""><job>plumber</job><pay>30000</pay></nz:item></nz:collection></nz:storage>";

        [TestInitialize]
        public void Init()
        {
            InitBase();
        }

        [TestMethod]
        public void MyTestMethod()
        {
            Assert.IsTrue(true);
        }
    }
}
