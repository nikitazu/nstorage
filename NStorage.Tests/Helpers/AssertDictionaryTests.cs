﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NStorage.Tests.Helpers
{
    [TestClass]
    public class AssertDictionaryTests
    {
        Dictionary<int, string> dictionary = new Dictionary<int, string>
        {
            { 1, "foo" },
            { 2, "bar" }
        };

        Dictionary<int, string> same = new Dictionary<int, string>
        {
            { 1, "foo" },
            { 2, "bar" }
        };

        Dictionary<int, string> sameAndMore = new Dictionary<int, string>
        {
            { 1, "foo" },
            { 2, "bar" },
            { 3, "cux" }
        };

        Dictionary<int, string> otherValue = new Dictionary<int, string>
        {
            { 1, "foo" },
            { 2, "bax" }
        };

        Dictionary<int, string> otherKey = new Dictionary<int, string>
        {
            { 0, "foo" },
            { 2, "bar" }
        };

        [TestMethod]
        public void TestSame()
        {
            AssertDictionary.HasExpectedKeysAndValues(this.dictionary, this.same);
        }

        [TestMethod]
        public void TestSameAndMore()
        {
            AssertDictionary.HasExpectedKeysAndValues(this.dictionary, this.sameAndMore);
        }

        [TestMethod]
        [ExpectedException(typeof(AssertFailedException))]
        public void TestOtherValue()
        {
            AssertDictionary.HasExpectedKeysAndValues(this.dictionary, this.otherValue);
        }

        [TestMethod]
        [ExpectedException(typeof(AssertFailedException))]
        public void TestOtherKey()
        {
            AssertDictionary.HasExpectedKeysAndValues(this.dictionary, this.otherKey);
        }

        [TestMethod]
        public void TestSameAreSame()
        {
            AssertDictionary.AreSame(this.dictionary, this.same);
        }

        [TestMethod]
        [ExpectedException(typeof(AssertFailedException))]
        public void TestSameAndMoreAreNotSame()
        {
            AssertDictionary.AreSame(this.dictionary, this.sameAndMore);
        }
    }
}
