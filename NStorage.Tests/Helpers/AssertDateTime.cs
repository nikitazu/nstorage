﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NStorage.Tests.Helpers
{
    public static class AssertDateTime
    {
        public static void AssertEqualUniversalDateTime(DateTime actual, DateTime? expected, string message = null)
        {
            AssertEqualUniversalDateTime(actual, expected.Value, message);
        }

        public static void AssertEqualUniversalDateTime(DateTimeOffset actual, DateTimeOffset? expected, string message = null)
        {
            AssertEqualUniversalDateTime(actual, expected.Value, message);
        }

        public static void AssertEqualUniversalDateTime(DateTime actual, DateTime expected, string message = null)
        {
            Assert.AreEqual(DateTimeKind.Utc, expected.Kind);
            Assert.AreEqual(actual, expected, message);
        }

        public static void AssertEqualUniversalDateTime(DateTimeOffset actual, DateTimeOffset expected, string message = null)
        {
            Assert.AreEqual(TimeSpan.Zero, expected.Offset);
            Assert.AreEqual(actual, expected, message);
        }
    }
}
