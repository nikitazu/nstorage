﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NStorage.Tests.Helpers
{
    public static class AssertDictionary
    {
        public static void HasExpectedKeysAndValues<TKey, TValue>(IReadOnlyDictionary<TKey, TValue> expected, IReadOnlyDictionary<TKey, TValue> actual)
        {
            foreach (var expectedItem in expected)
            {
                TValue actualValue;
                if (actual.TryGetValue(expectedItem.Key, out actualValue))
                {
                    Assert.AreEqual(expectedItem.Value, actualValue, "Dictionaries have different values for key: {0}", expectedItem.Key);
                }
                else
                {
                    Assert.Fail("One of dictionaries does not have key: {0}", expectedItem.Key);
                }
            }
        }

        public static void AreSame<TKey, TValue>(IReadOnlyDictionary<TKey, TValue> expected, IReadOnlyDictionary<TKey, TValue> actual)
        {
            HasExpectedKeysAndValues(expected, actual);
            HasExpectedKeysAndValues(actual, expected);
        }
    }
}
