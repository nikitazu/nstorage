﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NStorage.Tests
{
    [TestClass]
    public class StorageCollectionTests : StorageTestBase
    {
        StorageCollection collection;
        Dictionary<string, string> prepairedItem;
        Dictionary<string, string> prepairedItem2;
        Dictionary<string, string> changedItem;
        Dictionary<string, string> changes;
        Int64 prepairdItemId;

        [TestInitialize]
        public void Init()
        {
            InitBase();
            this.storage.AddCollection("foo");
            this.collection = storage["foo"];
            this.prepairedItem = new Dictionary<string, string>
            {
                { "name", "nikita" },
                { "age", "20" },
                { "likes", "tea" }
            };
            this.prepairedItem2 = new Dictionary<string, string>
            {
                { "name", "vovan" },
                { "age", "20" },
                { "likes", "coffee" }
            };
            this.changedItem = new Dictionary<string, string>
            {
                { "name", "alex" },
                { "age", "23" },
                { "likes", "tea" }
            };
            this.changes = new Dictionary<string, string>
            {
                { "name", "alex" },
                { "age", "23" }
            };

            this.prepairdItemId = this.collection.Insert(this.prepairedItem);
        }

        [TestMethod]
        public void TestInsert()
        {
            Assert.AreEqual(1, this.prepairdItemId);
        }

        [TestMethod]
        public void TestGet()
        {
            var itemFromCollection = this.collection[this.prepairdItemId];
            Helpers.AssertDictionary.AreSame(this.prepairedItem, itemFromCollection);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void TestRemove()
        {
            this.collection.Remove(this.prepairdItemId);
            var _ = this.collection[this.prepairdItemId];
        }

        [TestMethod]
        public void TestInsertOrReplace()
        {
            this.collection.InsertOrReplace(this.prepairdItemId, this.changes);
            var itemFromCollection = this.collection[this.prepairdItemId];
            Helpers.AssertDictionary.AreSame(this.changes, itemFromCollection);
        }

        [TestMethod]
        public void TestIdIncreasedOnInsertOrReplaceWithGaps()
        {
            this.collection.InsertOrReplace(5, this.prepairedItem2);
            Assert.AreEqual(6, this.collection.Insert(this.changedItem));
        }

        [TestMethod]
        public void TestGetAll()
        {
            this.collection.Insert(this.prepairedItem2);
            var items = this.collection.GetAll().ToArray();
            Assert.AreEqual(2, items.Length);
            Assert.AreEqual(this.prepairdItemId, items[0].ID);
            Assert.AreEqual(this.prepairdItemId+1, items[1].ID);
            Helpers.AssertDictionary.AreSame(this.prepairedItem, items[0].Value);
            Helpers.AssertDictionary.AreSame(this.prepairedItem2, items[1].Value);
        }
    }
}
