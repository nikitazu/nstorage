﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NStorage.Tests
{
    [TestClass]
    public class StorageTests : StorageTestBase
    {
        const string storageData = @"<?xml version=""1.0"" encoding=""utf-16""?><nz:storage xmlns:nz=""http://nikitazu.ru/nstorage""><nz:collection name=""myItems""><nz:item id=""1""><name>niki</name><age>20</age></nz:item><nz:item id=""2""><name>alex</name><age>30</age></nz:item></nz:collection><nz:collection name=""jobs""><nz:item id=""1""><job>plumber</job><pay>30000</pay></nz:item></nz:collection></nz:storage>";
        const string storageDataWithInjectionOfItemAsProperty = @"<?xml version=""1.0"" encoding=""utf-16""?><nz:storage xmlns:nz=""http://nikitazu.ru/nstorage""><nz:collection name=""jobs""><nz:item id=""1""><item>injection</item><job>plumber</job><pay>30000</pay></nz:item></nz:collection></nz:storage>";
        
        Dictionary<string, string> item1 = new Dictionary<string, string> { { "name", "niki" }, { "age", "20" } };
        Dictionary<string, string> item2 = new Dictionary<string, string> { { "name", "alex" }, { "age", "30" } };
        Dictionary<string, string> job1 = new Dictionary<string, string> { { "job", "plumber" }, { "pay", "30000" } };
        Dictionary<string, string> jobWithInjection = new Dictionary<string, string> { { "item", "injection" }, { "job", "plumber" }, { "pay", "30000" } };

        [TestInitialize]
        public void Init()
        {
            InitBase();
        }

        [TestMethod]
        public void TestAddCollection()
        {
            this.storage.AddCollection("foo");
            StorageCollection foo = this.storage["foo"];
            Assert.IsNotNull(foo);
        }

        [TestMethod]
        public void TestAddCollectionIfNotExists()
        {
            var foo1 = this.storage.AddCollectionIfNotExists("foo");
            var foo2 = this.storage.AddCollectionIfNotExists("foo");
            Assert.IsTrue(foo1 == foo2);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void TestRemoveCollection()
        {
            this.storage.AddCollection("foo");
            this.storage.RemoveCollection("foo");
            var _ = this.storage["foo"];
        }

        [TestMethod]
        public void TestWriteTo()
        {
            var myItems = this.storage.AddCollection("myItems");
            var jobs = this.storage.AddCollection("jobs");
            myItems.Insert(this.item1);
            myItems.Insert(this.item2);
            jobs.Insert(this.job1);

            Assert.AreEqual(storageData, this.storage.WriteToString());
        }

        [TestMethod]
        public void TestReadFrom()
        {
            this.storage.AddCollection("myItems");
            this.storage.AddCollection("jobs");
            this.storage.ReadFromString(storageData);
            Helpers.AssertDictionary.AreSame(this.item1, storage["myItems"][1]);
            Helpers.AssertDictionary.AreSame(this.item2, storage["myItems"][2]);
            Helpers.AssertDictionary.AreSame(this.job1, storage["jobs"][1]);
        }

        [TestMethod]
        public void TestReadFromDataWithInjectionOfItem()
        {
            this.storage.AddCollection("jobs");
            this.storage.ReadFromString(storageDataWithInjectionOfItemAsProperty);
            Helpers.AssertDictionary.AreSame(this.jobWithInjection, storage["jobs"][1]);
        }
    }
}
