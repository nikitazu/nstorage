﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.Tests.Helpers;

namespace NStorage.Tests.StorageDataTypes
{
    [TestClass]
    public class WritingDataNullableTests : StorageWithDataTestBase
    {
        [TestInitialize]
        public void Init()
        {
            InitBase();
        }

        // Nullable primitive types with data writing
        
        [TestMethod]
        public void TestIntNullableWithDataIsSaved()
        {
            Assert.AreEqual(dataInt, PrepairDataInStringFromColumn<FooIntNullable>(new FooIntNullable
            {
                Bar = 1
            }));
        }

        [TestMethod]
        public void TestDecimalNullableWithDataIsSaved()
        {
            Assert.AreEqual(dataDecimal, PrepairDataInStringFromColumn<FooDecimalNullable>(new FooDecimalNullable
            {
                Bar = 10.5m
            }));
        }

        [TestMethod]
        public void TestFloatNullableWithDataIsSaved()
        {
            Assert.AreEqual(dataFloat, PrepairDataInStringFromColumn<FooFloatNullable>(new FooFloatNullable
            {
                Bar = 10.5f
            }));
        }

        [TestMethod]
        public void TestDateTimeNullableWithDataIsSaved()
        {
            Assert.AreEqual(dataDateTime, PrepairDataInStringFromColumn<FooDateTimeNullable>(new FooDateTimeNullable
            {
                Bar = new DateTime(2010, 12, 30, 0, 0, 0, DateTimeKind.Utc)
            }));
        }

        [TestMethod]
        public void TestDateTimeOffsetNullableWithDataIsSaved()
        {
            Assert.AreEqual(dataDateTime, PrepairDataInStringFromColumn<FooDateTimeOffsetNullable>(new FooDateTimeOffsetNullable
            {
                Bar = new DateTimeOffset(2010, 12, 30, 0, 0, 0, TimeSpan.Zero)
            }));
        }

        [TestMethod]
        public void TestTimeSpanNullableWithDataIsSaved()
        {
            Assert.AreEqual(dataTimeSpan, PrepairDataInStringFromColumn<FooTimeSpanNullable>(new FooTimeSpanNullable
            {
                Bar = new TimeSpan(1, 2, 3)
            }));
        }

        // Nullable primitive types without data writing

        [TestMethod]
        public void TestIntNullableWithoutDataIsSaved()
        {
            Assert.AreEqual(dataNull, PrepairDataInStringFromColumn<FooIntNullable>(new FooIntNullable()));
        }

        [TestMethod]
        public void TestDecimalNullableWithoutDataIsSaved()
        {
            Assert.AreEqual(dataNull, PrepairDataInStringFromColumn<FooDecimalNullable>(new FooDecimalNullable()));
        }

        [TestMethod]
        public void TestFloatNullableWithoutDataIsSaved()
        {
            Assert.AreEqual(dataNull, PrepairDataInStringFromColumn<FooFloatNullable>(new FooFloatNullable()));
        }

        [TestMethod]
        public void TestDateTimeNullableWithoutDataIsSaved()
        {
            Assert.AreEqual(dataNull, PrepairDataInStringFromColumn<FooDateTimeNullable>(new FooDateTimeNullable()));
        }

        [TestMethod]
        public void TestDateTimeOffsetNullableWithoutDataIsSaved()
        {
            Assert.AreEqual(dataNull, PrepairDataInStringFromColumn<FooDateTimeOffsetNullable>(new FooDateTimeOffsetNullable()));
        }

        [TestMethod]
        public void TestTimeSpanNullableWithoutDataIsSaved()
        {
            Assert.AreEqual(dataNull, PrepairDataInStringFromColumn<FooTimeSpanNullable>(new FooTimeSpanNullable()));
        }
    }
}
