﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.Tests.Helpers;

namespace NStorage.Tests.StorageDataTypes
{
    [TestClass]
    public class ReadingDataNullableTests : StorageWithDataTestBase
    {
        [TestInitialize]
        public void Init()
        {
            InitBase();
        }

        // Nullable primitive types with data reading

        [TestMethod]
        public void TestIntNullable()
        {
            var col = PrepairDataInColumnFromString<FooIntNullable>(dataInt);
            Assert.AreEqual(1, col[1].Bar);
        }

        [TestMethod]
        public void TestDecimalNullable()
        {
            var col = PrepairDataInColumnFromString<FooDecimalNullable>(dataDecimal);
            Assert.AreEqual(10.5m, col[1].Bar);
        }

        [TestMethod]
        public void TestFloatNullable()
        {
            var col = PrepairDataInColumnFromString<FooFloatNullable>(dataFloat);
            Assert.AreEqual(10.5f, col[1].Bar);
        }

        [TestMethod]
        public void TestDateTimeNullable()
        {
            var col = PrepairDataInColumnFromString<FooDateTimeNullable>(dataDateTime);
            AssertDateTime.AssertEqualUniversalDateTime(new DateTime(2010, 12, 30), col[1].Bar);
        }

        [TestMethod]
        public void TestDateTimeOffsetNullable()
        {
            var col = PrepairDataInColumnFromString<FooDateTimeOffsetNullable>(dataDateTimeOffset);
            AssertDateTime.AssertEqualUniversalDateTime(new DateTimeOffset(2010, 12, 30, 0, 0, 0, TimeSpan.Zero), col[1].Bar);
        }

        [TestMethod]
        public void TestTimeSpanNullable()
        {
            var col = PrepairDataInColumnFromString<FooTimeSpanNullable>(dataTimeSpan);
            Assert.AreEqual(new TimeSpan(1, 2, 3), col[1].Bar);
        }

        // Nullable primitive types without data reading

        [TestMethod]
        public void TestIntNullableNull()
        {
            var col = PrepairDataInColumnFromString<FooIntNullable>(dataNull);
            Assert.IsNull(col[1].Bar);
        }

        [TestMethod]
        public void TestDecimalNullableNull()
        {
            var col = PrepairDataInColumnFromString<FooDecimalNullable>(dataNull);
            Assert.IsNull(col[1].Bar);
        }

        [TestMethod]
        public void TestFloaNullabletNull()
        {
            var col = PrepairDataInColumnFromString<FooFloatNullable>(dataNull);
            Assert.IsNull(col[1].Bar);
        }

        [TestMethod]
        public void TestDateTimeNullableNull()
        {
            var col = PrepairDataInColumnFromString<FooDateTimeNullable>(dataNull);
            Assert.IsNull(col[1].Bar);
        }

        [TestMethod]
        public void TestDateTimeOffsetNullableNull()
        {
            var col = PrepairDataInColumnFromString<FooDateTimeOffsetNullable>(dataNull);
            Assert.IsNull(col[1].Bar);
        }

        [TestMethod]
        public void TestTimeSpanNullableNull()
        {
            var col = PrepairDataInColumnFromString<FooTimeSpanNullable>(dataNull);
            Assert.IsNull(col[1].Bar);
        }
    }
}
