﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.Tests.Helpers;

namespace NStorage.Tests.StorageDataTypes
{
    [TestClass]
    public class ReadingDataTests : StorageWithDataTestBase
    {
        [TestInitialize]
        public void Init()
        {
            InitBase();
        }

        [TestMethod]
        public void TestInt()
        {
            var col = PrepairDataInColumnFromString<FooInt>(dataInt);
            Assert.AreEqual(1, col[1].Bar);
        }

        [TestMethod]
        public void TestDecimal()
        {
            var col = PrepairDataInColumnFromString<FooDecimal>(dataDecimal);
            Assert.AreEqual(10.5m, col[1].Bar);
        }

        [TestMethod]
        public void TestFloat()
        {
            var col = PrepairDataInColumnFromString<FooFloat>(dataFloat);
            Assert.AreEqual(10.5f, col[1].Bar);
        }

        [TestMethod]
        public void TestDateTime()
        {
            var col = PrepairDataInColumnFromString<FooDateTime>(dataDateTime);
            AssertDateTime.AssertEqualUniversalDateTime(new DateTime(2010, 12, 30, 0, 0, 0, DateTimeKind.Utc), col[1].Bar);
        }

        [TestMethod]
        public void TestDateTimeOffset()
        {
            var col = PrepairDataInColumnFromString<FooDateTimeOffset>(dataDateTimeOffset);
            AssertDateTime.AssertEqualUniversalDateTime(new DateTimeOffset(2010, 12, 30, 0, 0, 0, TimeSpan.Zero), col[1].Bar);
        }

        [TestMethod]
        public void TestTimeSpan()
        {
            var col = PrepairDataInColumnFromString<FooTimeSpan>(dataTimeSpan);
            Assert.AreEqual(new TimeSpan(1, 2, 3), col[1].Bar);
        }

        [TestMethod]
        public void TestString()
        {
            var col = PrepairDataInColumnFromString<FooString>(dataString);
            Assert.AreEqual("foo", col[1].Bar);
        }

        [TestMethod]
        public void TestStringEmpty()
        {
            var col = PrepairDataInColumnFromString<FooString>(dataStringEmpty);
            Assert.AreEqual(string.Empty, col[1].Bar);
        }

        [TestMethod]
        public void TestStringNull()
        {
            var col = PrepairDataInColumnFromString<FooString>(dataNull);
            Assert.IsNull(col[1].Bar);
        }
    }
}
