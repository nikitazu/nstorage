﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NStorage.Tests.StorageDataTypes
{
    public abstract class StorageWithDataTestBase : StorageTestBase
    {
        protected const string dataStart = @"<?xml version=""1.0"" encoding=""utf-16""?><nz:storage xmlns:nz=""http://nikitazu.ru/nstorage""><nz:collection name=""x""><nz:item id=""1""><Bar>";
        protected const string dataEnd = @"</Bar></nz:item></nz:collection></nz:storage>";

        protected const string dataInt = dataStart + "1" + dataEnd;
        protected const string dataDecimal = dataStart + "10.5" + dataEnd;
        protected const string dataFloat = dataStart + "10.5" + dataEnd;
        protected const string dataDateTime = dataStart + "2010-12-30 00:00:00Z" + dataEnd;
        protected const string dataDateTimeOffset = dataStart + "2010-12-30 00:00:00Z" + dataEnd;
        protected const string dataTimeSpan = dataStart + "01:02:03" + dataEnd;
        protected const string dataString = dataStart + "foo" + dataEnd;
        protected const string dataStringEmpty = dataStart + "" + dataEnd;

        protected const string dataNull = @"<?xml version=""1.0"" encoding=""utf-16""?><nz:storage xmlns:nz=""http://nikitazu.ru/nstorage""><nz:collection name=""x""><nz:item id=""1""><Bar isnull=""true"" /></nz:item></nz:collection></nz:storage>";
    }

    public class FooInt { public int Bar { get; set; } }
    public class FooDecimal { public decimal Bar { get; set; } }
    public class FooFloat { public float Bar { get; set; } }
    public class FooDateTime { public DateTime Bar { get; set; } }
    public class FooDateTimeOffset { public DateTimeOffset Bar { get; set; } }
    public class FooTimeSpan { public TimeSpan Bar { get; set; } }
    public class FooString { public string Bar { get; set; } }

    public class FooIntNullable { public int? Bar { get; set; } }
    public class FooDecimalNullable { public decimal? Bar { get; set; } }
    public class FooFloatNullable { public float? Bar { get; set; } }
    public class FooDateTimeNullable { public DateTime? Bar { get; set; } }
    public class FooDateTimeOffsetNullable { public DateTimeOffset? Bar { get; set; } }
    public class FooTimeSpanNullable { public TimeSpan? Bar { get; set; } }
}
