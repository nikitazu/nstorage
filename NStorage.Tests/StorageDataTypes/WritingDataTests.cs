﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NStorage.Tests.Helpers;

namespace NStorage.Tests.StorageDataTypes
{
    [TestClass]
    public class WritingDataTests : StorageWithDataTestBase
    {
        [TestInitialize]
        public void Init()
        {
            InitBase();
        }

        [TestMethod]
        public void TestIntIsSaved()
        {
            Assert.AreEqual(dataInt, PrepairDataInStringFromColumn<FooInt>(new FooInt
            {
                Bar = 1
            }));
        }

        [TestMethod]
        public void TestDecimalIsSaved()
        {
            Assert.AreEqual(dataDecimal, PrepairDataInStringFromColumn<FooDecimal>(new FooDecimal
            {
                Bar = 10.5m
            }));
        }
        
        [TestMethod]
        public void TestFloatIsSaved()
        {
            Assert.AreEqual(dataFloat, PrepairDataInStringFromColumn<FooFloat>(new FooFloat
            {
                Bar = 10.5f
            }));
        }

        [TestMethod]
        public void TestDateTimeIsSaved()
        {
            Assert.AreEqual(dataDateTime, PrepairDataInStringFromColumn<FooDateTime>(new FooDateTime
            {
                Bar = new DateTime(2010, 12, 30, 0, 0, 0, DateTimeKind.Utc)
            }));
        }

        [TestMethod]
        public void TestDateTimeOffsetIsSaved()
        {
            Assert.AreEqual(dataDateTime, PrepairDataInStringFromColumn<FooDateTimeOffset>(new FooDateTimeOffset
            { 
                Bar = new DateTimeOffset(2010, 12, 30, 0, 0, 0, TimeSpan.Zero)
            }));
        }

        [TestMethod]
        public void TestTimeSpanIsSaved()
        {
            Assert.AreEqual(dataTimeSpan, PrepairDataInStringFromColumn<FooTimeSpan>(new FooTimeSpan
            {
                Bar = new TimeSpan(1, 2, 3)
            }));
        }
    }
}
