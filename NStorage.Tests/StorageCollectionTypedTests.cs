﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NStorage.Tests
{
    class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }

    [TestClass]
    public class StorageCollectionTypedTests : StorageTestBase
    {
        StorageCollection<Person> people;
        Person prepairedItem = new Person { Name = "nikita", Age = 20 };
        Person prepairedItem2 = new Person { Name = "vovan", Age = 20 };
        Person changedItem = new Person { Name = "alex", Age = 30 };
        Int64 prepairdItemId;

        [TestInitialize]
        public void Init()
        {
            InitBase();
            this.people = this.storage.AddCollection<Person>();
            this.prepairdItemId = this.people.Insert(this.prepairedItem);
        }

        [TestMethod]
        public void TestInsertTyped()
        {
            Assert.AreEqual(1, this.prepairdItemId);
        }

        [TestMethod]
        public void TestGetTyped()
        {
            Person itemFromCollection = this.people[this.prepairdItemId];
            Assert.AreEqual(this.prepairedItem.Name, itemFromCollection.Name);
            Assert.AreEqual(this.prepairedItem.Age, itemFromCollection.Age);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void TestRemoveTyped()
        {
            this.people.Remove(this.prepairdItemId);
            var _ = this.people[this.prepairdItemId];
        }

        [TestMethod]
        public void TestInsertOrReplaceTyped()
        {
            this.people.InsertOrReplace(this.prepairdItemId, this.prepairedItem2);
            var itemFromCollection = this.people[this.prepairdItemId];
            Assert.AreEqual(this.prepairedItem2.Name, itemFromCollection.Name);
            Assert.AreEqual(this.prepairedItem2.Age, itemFromCollection.Age);
        }

        [TestMethod]
        public void TestIdIncreasedOnInsertOrReplaceWithGapsTyped()
        {
            this.people.InsertOrReplace(5, this.prepairedItem2);
            Assert.AreEqual(6, this.people.Insert(this.changedItem));
        }

        [TestMethod]
        public void TestGetAllTyped()
        {
            this.people.Insert(this.prepairedItem2);
            var items = this.people.GetAll().ToArray();

            Assert.AreEqual(2, items.Length);
            Assert.AreEqual(this.prepairdItemId, items[0].ID);
            Assert.AreEqual(this.prepairdItemId + 1, items[1].ID);

            Assert.AreEqual(this.prepairedItem.Name, items[0].Value.Name);
            Assert.AreEqual(this.prepairedItem.Age, items[0].Value.Age);

            Assert.AreEqual(this.prepairedItem2.Name, items[1].Value.Name);
            Assert.AreEqual(this.prepairedItem2.Age, items[1].Value.Age);
        }
    }
}
