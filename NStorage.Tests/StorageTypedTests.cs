﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NStorage.Tests
{
    class Foo
    {
        public string Bar { get; set; }
    }

    [TestClass]
    public class StorageTypedTests : StorageTestBase
    {
        const string storageDataTyped = @"<?xml version=""1.0"" encoding=""utf-16""?><nz:storage xmlns:nz=""http://nikitazu.ru/nstorage""><nz:collection name=""NStorage.Tests.Person""><nz:item id=""1""><Name>niki</Name><Age>20</Age></nz:item><nz:item id=""2""><Name>alex</Name><Age>30</Age></nz:item></nz:collection><nz:collection name=""otherPeople""><nz:item id=""1""><Name>dimas</Name><Age>23</Age></nz:item></nz:collection></nz:storage>";
        const string storageDataTypedWithNullData = @"<?xml version=""1.0"" encoding=""utf-16""?><nz:storage xmlns:nz=""http://nikitazu.ru/nstorage""><nz:collection name=""NStorage.Tests.Person""><nz:item id=""1""><Name isnull=""true"" /><Age>0</Age></nz:item></nz:collection><nz:collection name=""otherPeople"" /></nz:storage>";

        Person person1 = new Person { Name = "niki", Age = 20 };
        Person person2 = new Person { Name = "alex", Age = 30 };
        Person otherPerson = new Person { Name = "dimas", Age = 23 };
        Person nullNamePerson = new Person { Name = null, Age = 0 };

        StorageCollection<Person> people;
        StorageCollection<Person> otherPeople;

        [TestInitialize]
        public void Init()
        {
            InitBase();
            this.people = this.storage.AddCollection<Person>();
            this.otherPeople = this.storage.AddCollection<Person>("otherPeople");
        }

        [TestMethod]
        public void TestAddCollectionTyped()
        {
            Assert.IsNotNull(this.people);
            Assert.IsTrue(this.people.GetType() == typeof(StorageCollection<Person>), "people actual type is: {0}", this.people.GetType().FullName);
        }

        [TestMethod]
        public void TestAddCollectionIfNotExistsTyped()
        {
            var foo1 = this.storage.AddCollectionIfNotExists<Foo>();
            var foo2 = this.storage.AddCollectionIfNotExists<Foo>();
            Assert.IsTrue(foo1 == foo2);
            Assert.IsTrue(foo1.GetType() == typeof(StorageCollection<Foo>), "Foo actual type is: {0}", foo1.GetType().FullName);
        }

        [TestMethod]
        public void TestGetCollectionTyped()
        {
            var col1 = this.storage.Get<Person>();
            var col2 = this.storage.Get<Person>("otherPeople");

            Assert.IsTrue(col1 == this.people);
            Assert.IsTrue(col2 == this.otherPeople);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void TestRemoveCollectionTyped()
        {
            this.storage.AddCollection<Foo>();
            this.storage.RemoveCollection<Foo>();
            var _ = this.storage.Get<Foo>();
        }

        [TestMethod]
        public void TestWriteToTyped()
        {
            this.people.Insert(this.person1);
            this.people.Insert(this.person2);
            this.otherPeople.Insert(this.otherPerson);
            Assert.AreEqual(storageDataTyped, this.storage.WriteToString());
        }

        [TestMethod]
        public void TestReadFromTyped()
        {
            this.storage.ReadFromString(storageDataTyped);
            Assert.AreEqual(this.person1.Name, this.people[1].Name);
            Assert.AreEqual(this.person1.Age, this.people[1].Age);
            Assert.AreEqual(this.person2.Name, this.people[2].Name);
            Assert.AreEqual(this.person2.Age, this.people[2].Age);
            Assert.AreEqual(this.otherPerson.Name, this.otherPeople[1].Name);
            Assert.AreEqual(this.otherPerson.Age, this.otherPeople[1].Age);
        }

        [TestMethod]
        public void TestWriteToTypedWithNullData()
        {
            this.people.Insert(this.nullNamePerson);
            Assert.AreEqual(storageDataTypedWithNullData, this.storage.WriteToString());
        }
    }
}
