﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NStorage.Tests
{
    public abstract class StorageTestBase
    {
        protected Storage storage;

        protected void InitBase()
        {
            this.storage = new Storage();
        }

        protected string PrepairDataInStringFromColumn<T>(T data)
        {
            this.storage.AddCollection<T>("x").InsertOrReplace(1, data);
            return this.storage.WriteToString();
        }

        protected StorageCollection<T> PrepairDataInColumnFromString<T>(string data)
        {
            var col = this.storage.AddCollection<T>("x");
            this.storage.ReadFromString(data);
            return col;
        }
    }
}
