# README #

### How do I get set up? ###

* Use package manager console in Microsoft Visual Studio
* PM> Install-Package NStorage


### Who do I talk to? ###

* Nikita Zuev <nikitazu@gmail.com>

### Usage - saving data ###

```
#!c#

// import NStorage namespace
using NStorage;

// create storage object
var storage = new Storage();

// insert some data
var myItems = this.storage.AddCollection("myItems");
var item1 = new Dictionary<string, string> { { "name", "niki" }, { "age", "20" } };
var item2 = new Dictionary<string, string> { { "name", "alex" }, { "age", "30" } };
myItems.Insert(item1);
myItems.Insert(item2);

// save data to file
using (var writer = new StreamWriter(System.IO.File.OpenWrite("path/to/file")))
{
  this.storage.WriteTo(writer);
}
```


### Usage - loading data ###

```
#!c#

// import NStorage namespace
using NStorage;

// create storage object
var storage = new Storage();

// load data from file
using (var reader = new StreamReader(System.IO.File.OpenRead("path/to/file")))
{
  this.storage.AddCollection("myItems"); // only added collections will be read
  this.storage.ReadFrom(reader);
}

// select some data
var myItems = storage["myItems"];
foreach(var item in myItems.GetAll())
{
  Console.WriteLine("Item #{0} with properties {1} {2}", item.ID, item.Value["name"], item.Value["age"]);
}

```

### Usage - creating typed data ###

```
#!c#

// our type
class MyItem
{
  public Name { get; set; }
  public Age { get; set; }
}

// insert some data
var myItems = this.storage.AddCollection<MyItem>();
var item1 = new MyItem { Name = "niki",  Age = 20 };
var item2 = new MyItem { Name = "alex", Age = 30 };
myItems.Insert(item1);
myItems.Insert(item2);

```


### Usage - getting typed data ###

```
#!c#

// our type
class MyItem
{
  public Name { get; set; }
  public Age { get; set; }
}

// select some data
var myItems = storage.Get<MyItem>();
foreach(var item in myItems.GetAll())
{
  Console.WriteLine("Item #{0} with properties {1} {2}", item.ID, item.Value.Name, item.Value.Age);
}

```